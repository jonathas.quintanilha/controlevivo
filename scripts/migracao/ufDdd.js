﻿$(document).on('click', '#ufSelector, a.linkModalUFDDD', function (event) {
    $('#selecionaEstadoModal').modal('show');
    $('body').css('padding-right', '0');
    event.preventDefault();
});

$(document).on("change", "#modalUf", function modalUfChange(event) {
    var uf = $(this).val();
    $("#hdnUF").val(uf);
    simplePostUF(event, "POST", "/RetornarDDDsPorUF", $(this).val(), retornoOnChange);
});

$(document).on("click", "#btnSelecionarUFDDD", function linkSelecionaDDDUFClick(event) {
    var ddd = $("#modalDDD option:selected").val();
    var ufOrigem = $("#modalUf option:selected").val();
    $("#hdnDDD").val(ddd);
    var dddParameter = { ddd: ddd, ufOrigem: ufOrigem };
    simplePost(event, "POST", "/RecarregarPlanosPorDDD", dddParameter, recarregarPlanosPorDDD);
});

$(document).on("blur", "#ddd_servico", function carregaPlanos(event) {
    $("#hdnDDD").val($(this).val());
    simplePost(event, "POST", "/RecarregarPlanosPorDDD", { ddd: $(this).val() }, recarregarPlanosPorDDD);
});

$('.close-modal').on('click', function () {
    $('#selecionaEstadoModal').modal('hide');
});