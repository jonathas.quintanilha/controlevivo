﻿// Handler for .ready() called.
$(function () {

    var ddd = getParameterByName("ddd");
    if (ddd) {
        $("#ddd_servico").val(ddd);
    }

    $("#formLead").validate({
        errorPlacement: function (error, element) {
            return true; //DO NOT SHOW ERROR MESSAGE
        },
        onfocusout: function (element) {
            $(element).valid();
        },
        onkeyup: function (element) {
            if ($(element).val() === "") {
                $(element).valid();
            }
        },
        rules: {
            nome: {
                required: true,
                oneword: true
            },
            email: {
                email: true
            }
        },
        messages: {
            ddd_servico: {
                required: "",
                regex: "",
                maxlength: "",
                minlength: ""
            },
            linha_servico: {
                required: "",
                regex: "",
                maxlength: "",
                minlength: ""
            },
            nome: {
                required: ""
            },
            email: {
                required: "",
                maxlength: ""
            }
        }
    });


$("#nome").keyup(function(e) {
    if (/[0-9@!#\$\^´`%&*()+=\-_\[\]\\\;,\.\/\{\}\|\":<>\?¨]+$/g.test(this.value)) {
        // Filter non-digits from input value.
        this.value = this.value.replace(/[0-9@!#\$\^´`%&*()+=\-_\[\]\\\;,\.\/\{\}\|\":<>\?¨]+$/g, '');
    }
});

    $("#ddd_servico").keyup(function (e) {
        if (/\D/g.test(this.value)) {
            // Filter non-digits from input value.
            this.value = this.value.replace(/\D/g, '');
        }
    });
    $("#linha_servico").keyup(function (e) {
        if (/\D/g.test(this.value)) {
            // Filter non-digits from input value.
            this.value = this.value.replace(/\D/g, '');
        }
    });

    $('#ufSelector').css('cursor', 'pointer');

    //Regra de formação de números de celular
    // $("#linhaServico").rules("add", { regex: /^([9][4-9]{1}[0-9]{3}[0-9]{4})|([7-9]{1}[0-9]{3}[0-9]{4})$/mg });
    // O trecho da regra acima '|([7-9]{1}[0-9]{3}[0-9]{4})$/mg }' está deixando passar números inválidos. Por isso a remoção.
    // Ex: /^([9][4-9]{1}[0-9]{3}[0-9]{4})|([7-9]{1}[0-9]{3}[0-9]{4})$/mg.test("188426829")
    $("#linha_servico").rules("add", { regex: /^([9][4-9]{1}[0-9]{3}[0-9]{4})$/mg });

    // DDDs válidos
    $("#ddd_servico").rules("add", { regex: /^(11|12|13|14|15|16|17|18|19|22|21|24|27|28|31|32|33|34|35|37|38|41|42|43|44|45|46|47|48|49|51|53|54|55|61|62|63|64|65|66|67|68|69|71|73|74|75|77|79|81|82|83|84|85|86|87|88|89|91|92|93|94|95|96|97|98|99)$/mg });

    $("#email").rules("add", { regex: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/mg });

    //Muda o foco para o telefone caso o DDD seja válido
    $(document).on('keyup', '#ddd_servico', function (event) {
        var value = $(this).val();
        if ((value.length == 2) && ($("#ddd_servico").valid())) {
            $("#linha_servico").focus();
        }
    });


    //Regra de formação de números de celular
    //$("#telefone").rules("add", { regex: /^(11|12|13|14|15|16|17|18|19|22|21|24|27|28|31|32|33|34|35|37|38|41|42|43|44|45|46|47|48|49|51|53|54|55|61|62|63|64|65|66|67|68|69|71|73|74|75|77|79|81|82|83|84|85|86|87|88|89|91|92|93|94|95|96|97|98|99)([9][4-9]{1}[0-9]{3}[0-9]{4})|(11|12|13|14|15|16|17|18|19|22|21|24|27|28|31|32|33|34|35|37|38|41|42|43|44|45|46|47|48|49|51|53|54|55|61|62|63|64|65|66|67|68|69|71|73|74|75|77|79|81|82|83|84|85|86|87|88|89|91|92|93|94|95|96|97|98|99)([5-9]{1}[0-9]{3}[0-9]{4})$/mg });

    //$('#formLead').on('submit', function (event) {
    //    if ($(this).valid()) {
    //        $("#btnSubmit").attr("disabled", true).val("ENVIANDO...");
    //    }
    //}); //Submit

    btnContratarPlanoEvento();

    $(document).on('click', '#btnBottom1', function (event) {
        $('html,body').scrollTop(0);
        $("#nome").focus();
        event.preventDefault();
    });
    $(document).on('click', '#btnBottom2', function (event) {
        $('html,body').scrollTop(0);
        $("#nome").focus();
        event.preventDefault();
    });

    var oBtnSubmitLead = $("#btnSubmitLead");
    var form = $('#formLead');

    function retornoLead(data) {
        form.removeAttr('disabled');
        oBtnSubmitLead.prop("disabled", false).val("ESCOLHA SEU PLANO");
        if (data.r === "OK") {
            window.location = data.d;
        } else {
            if (data.m) alert(data.m);
        }
        if (event !== undefined)
        {
            event.preventDefault();
        }

    }

    form.on('submit', function (event) {
        if ($(this).valid()) {
            ajaxPost(event, $(this), oBtnSubmitLead, retornoLead);
            event.preventDefault();
        }
    });

    carregarTextoDestaque();
    //recarregarPlanos();
});

function btnContratarPlanoEvento() {
    if ($("#btnBottom2")) {
        $(".btn-contratar-plano").not("[id = 'btnSelecionarUFDDD']").remove();
    }
}

function carregarTextoDestaque() {
    $("#estadoDestaque").text(obtemEstadoPelaSigla($("#hdnUfDestaque").val()));
    $("#dddDestaque").text($("#hdnDddDestaque").val());
    $("span.infoNomeUf").text(obtemEstadoPelaSigla($("#hdnUfDestaque").val()));
    $("span.infoDDD").text($("#hdnDddDestaque").val());
}

function recarregarPlanosPorDDD(data) {
    if (data.r === "OK") {
        $("#quadroPlanosTopo").html(data.viewQuadroPlanosTopo);
        $("#quadroPlanosTopoMobile").html(data.viewQuadroPlanosTopo);
        $("#partialPlanos").html(data.viewPartialPlanos);
        // $("#partialServicosDigitais").html(data.viewPartialServicosDigitais);
        $("#ufHeader").text(data.nomeUf.toUpperCase());
        $("#modalUf").val(data.uf);
        $("#estadoDestaque").text(data.nomeUf);
        $("#dddDestaque").text(data.ddd);
        $("span.menorValorPlano").text(data.menorValorPlano);
        $("span.maiorValorPlano").text(data.maiorValorPlano);
        $("span.infoNomeUf").text(data.nomeUf);
        $("span.infoDDD").text(data.ddd);

        var str = "";
        var dddSplit = data.ddds.split(',');
        $(dddSplit).each(function foreachDdds(i, val) {
            str += "<option value='" + val.trim() + "'>" + val + "</option>";
        });
        $("#modalDDD").html(str);
        $("#modalDDD").val(data.ddd).change();

        $("#dddHeader").text(data.ddd);
        $("#ddd_servico").val(data.ddd);

        $("#selecionaEstadoModal").modal("hide");
        btnContratarPlanoEvento();
        filterServiceDigitalList(data.ddd);

    } else {
        console.log(data.m);
    }
}

function filterServiceDigitalList(ddd) {
    var ddds = ddd_service_filter || [];

    jQuery(".bloco-servicos-digitais").html("").addClass("owl-carousel owl-theme");
    if(ddds.indexOf(parseInt(ddd)) > -1) {
        jQuery(".hidemServiceItens .item").clone().appendTo(".bloco-servicos-digitais");
    } else {
        jQuery(".hidemServiceItens .item:not(.partial-service-item)").clone().appendTo(".bloco-servicos-digitais");
    }

    jQuery('.owl-carousel.bloco-servicos-digitais')
        .trigger('destroy.owl.carousel')
        .owlCarousel({
        loop: true,
        margin: 20,
        responsiveClass: true,

        responsive: {
            0: {
                items: 1,
                nav: false
            },
            700: {
                items: 2,
                nav: false
            },
            900: {
                items: 3,
                loop: false
            }
        }
    });
}

function retornoOnChange(data) {
    if (data.r === "OK") {
        var str = "";
        var dddSplit = data.ddds.split(',');

        $(dddSplit).each(function foreachDdds(i, val) {
            str += "<option value='" + val + "'>" + val + "</option>";
        });

        $("#modalDDD").html(str);
    }
}

$(document).on('click', '#regulamentoPos', function (event) {
    var ddd = $("#dddHeader").text();
    $.ajax({
        type: 'GET',
        url: '/ObterRegulamento',
        data: { ddd: ddd },
        async: false,
        timeout: 180000,
        success: abrirPdf,
        error: function (xhr, status, error) {
            var err = status + ", " + error;
            console.log("Request Failed: " + err);
        }
    });


    function abrirPdf(data) {
        if (data.r === "OK" && data.pdf) {
            window.open(data.pdf, '_blank');
        }
    }
    event.preventDefault();

});