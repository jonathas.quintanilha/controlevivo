﻿var eventoClick = (navigator.userAgent.match(/iPad/i)) ? "touchstart" : "click";

function ajaxPost(event, $form, btnSubmit, successCallback) {
    if ($form.valid()) {
        btnSubmit.attr("disabled", true).html("Processando...");
        var frmData = $form.serialize();

        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: frmData,
            timeout: 180000,
            success: successCallback,
            beforeSend: function() { $form.attr('disabled', 'disabled'); },
            error: function (xhr, status, error) {
                var err = status + ", " + error;
                console.log("Request Failed: " + err);
                trataErroRetorno(btnSubmit);
            }
        });
    };

    event.preventDefault();

}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function ajaxFormularioPost(event, $form, successCallback) {
    if ($form.valid()) {
        var frmData = $form.serialize();

        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: frmData,
            timeout: 180000,
            beforeSend: function () { $form.attr('disabled'); },
            success: successCallback,
            error: function (xhr, status, error) {
                $form.removeAttr('disabled');
                var err = status + ", " + error;
                console.log("Request Failed: " + err);
            }
        });
    };

    event.preventDefault();

}

function simplePost(event, type, url, data, successCallback) {
    $.ajax({
        type: type,
        url: url,
        data: data,
        dataType: "json",
        timeout: 180000,
        success: successCallback,
        error: function (xhr, status, error) {
            var err = status + ", " + error;
            console.log("Request Failed: " + err);
        }
    });
    event.preventDefault();
}

function simplePostUF(event, type, url, data, successCallback) {
    $.ajax({
        type: type,
        url: url,
        data: { uf: data },
        dataType: "json",
        timeout: 180000,
        success: successCallback,
        error: function (xhr, status, error) {
            var err = status + ", " + error;
            console.log("Request Failed: " + err);
        }
    });
    event.preventDefault();
}

function simplePostReenviarSMS(event, type, url, data, successCallback) {
    $.ajax({
        type: type,
        url: url,
        dataType: "json",
        timeout: 180000,
        success: successCallback,
        error: function (xhr, status, error) {
            var err = status + ", " + error;
            console.log("Request Failed: " + err);
        }
    });
    event.preventDefault();

}


function ajaxPostNoValidate(event, $form, btnSubmit, successCallback) {

    btnSubmit.attr("disabled", true).html("Processando...");
    var frmData = $form.serialize();

    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: frmData,
        timeout: 180000,
        success: successCallback,
        error: function (xhr, status, error) {
            var err = status + ", " + error;
            console.log("Request Failed: " + err);
        }
    });

    event.preventDefault();

}

function ajaxPostMudarPlano(event, type, url, skuPlano, codCatalogo, dispositivo, ddd, successCallback) {
    $.ajax({
        type: type,
        url: url,
        data: { skuPlano: skuPlano, codCatalogo: codCatalogo, dispositivo: dispositivo, ddd: ddd },
        dataType: "json",
        timeout: 180000,
        success: successCallback,
        error: function (xhr, status, error) {
            var err = status + ", " + error;
            console.log("Request Failed: " + err);
        }
    });
    event.preventDefault();
}

function ajaxPostMudarPlanoLeadMobile(event, type, url, successCallback) {
    $.ajax({
        type: type,
        url: url,
        data: {},
        dataType: "json",
        timeout: 180000,
        success: successCallback,
        error: function (xhr, status, error) {
            var err = status + ", " + error;
            console.log("Request Failed: " + err);
        }
    });
    event.preventDefault();
}

function ajaxPostCancelarEdicaoDadosPessoais(event, type, url, dispositivo, successCallback) {
    $.ajax({
        type: type,
        url: url,
        data: { dispositivo: dispositivo },
        dataType: "json",
        timeout: 180000,
        success: successCallback,
        error: function (xhr, status, error) {
            var err = status + ", " + error;
            console.log("Request Failed: " + err);
        }
    });
    event.preventDefault();
}


function pageviewGtm(pageViewName) {
    dataLayer.push({ 'event': 'pageview', 'pageview': pageViewName });
}

function obtemSiglaEstado(estado) {
    switch (estado) {
        case "Acre":
            return "AC";
        case "Alagoas":
            return "AL";
        case "Amapá":
            return "AP";
        case "Amazonas":
            return "AM";
        case "Bahia":
            return "BA";
        case "Ceará":
            return "CE";
        case "Distrito Federal":
            return "DF";
        case "Espiríto Santo":
            return "ES";
        case "Goiás":
            return "GO";
        case "Maranhão":
            return "MA";
        case "Mato Grosso":
            return "MT";
        case "Mato Grosso do Sul":
            return "MS";
        case "Minas Gerais":
            return "MG";
        case "Pará":
            return "PA";
        case "Paraíba":
            return "PB";
        case "Paraná":
            return "PR";
        case "Pernambuco":
            return "PE";
        case "Piauí":
            return "PI";
        case "Rio de Janeiro":
            return "RJ";
        case "Rio Grande do Norte":
            return "RN";
        case "Rio Grande do Sul":
            return "RS";
        case "Rondônia":
            return "RO";
        case "Roraima":
            return "RR";
        case "Santa Catarina":
            return "SC";
        case "São Paulo":
            return "SP";
        case "Sergipe":
            return "SE";
        case "Tocantins":
            return "TO";
        default:
            return "IN";
    }
}

function obtemEstadoPelaSigla(sigla) {
    switch (sigla) {
        case "AC":
            return "Acre";
        case "AL":
            return "Alagoas";
        case "AP":
            return "Amapá";
        case "AM":
            return "Amazonas";
        case "BA":
            return "Bahia";
        case "CE":
            return "Ceará";
        case "DF":
            return "Distrito Federal";
        case "ES":
            return "Espírito Santo";
        case "GO":
            return "Goiás";
        case "MA":
            return "Maranhão";
        case "MT":
            return "Mato Grosso";
        case "MS":
            return "Mato Grosso do Sul";
        case "MG":
            return "Minas Gerais";
        case "PA":
            return "Pará";
        case "PB":
            return "Paraíba";
        case "PR":
            return "Paraná";
        case "PE":
            return "Pernambuco";
        case "PI":
            return "Piauí";
        case "RJ":
            return "Rio de Janeiro";
        case "RN":
            return "Rio Grande do Norte";
        case "RS":
            return "Rio Grande do Sul";
        case "RO":
            return "Rondônia";
        case "RR":
            return "Roraima";
        case "SC":
            return "Santa Catarina";
        case "SP":
            return "São Paulo";
        case "SE":
            return "Sergipe";
        case "TO":
            return "Tocantins";
        default:
            return "IN";
    }
}

// Procura um parâmetro na URL.
// http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

/* FUNCTION PARA TRATAMENTO DO RETORNO DE ERRO DO AJAX - NESTE CASO APENAS LIBERAMOS BOTÃO PARA NOVO POST */
function trataErroRetorno(btnSubmit) {
    switch ($(btnSubmit).attr("id")) {
        case "btnSubmitDadosPessoais":
            btnSubmit.attr("disabled", false).html("Continuar");
            break;
        case "btnConfirmarSMS":
            btnSubmit.attr("disabled", false).html("Confirmar código");
            break;
        default:
            btnSubmit.attr("disabled", false).html("Finalizar");
            break;
    }
}

(function (window) {
    if (window.location !== window.top.location) {
        window.top.location = window.location;
    }
})(this);