(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: PT (Portuguese; português)
 * Region: BR (Brazil)
 */
$.extend($.validator.messages, {
	required: "Este campo &eacute; obrigat&oacute;rio.",
	remote: "Por favor, corrija este campo.",
	email: "Digite um endere&ccedil;o de email v&aacute;lido.",
	url: "Digite uma URL v&aacute;lida.",
	date: "Digite uma data v&aacute;lida.",
	dateISO: "Digite uma data v&aacute;lida (ISO).",
	number: "Digite um n&uacute;mero v&aacute;lido.",
	digits: "Digite somente d&iacute;gitos.",
	creditcard: "Digite um cart&atilde;o de cr&eacute;dito v&aacute;lido.",
	equalTo: "Digite o mesmo valor novamente.",
	extension: "Digite um valor com uma extens&atilde;o v&aacute;lida.",
	maxlength: $.validator.format("Digite n&atilde;o mais que {0} caracteres."),
	minlength: $.validator.format("Mínimo {0} caracteres."),
	rangelength: $.validator.format("Digite um valor entre {0} e {1} caracteres de comprimento."),
	range: $.validator.format("Digite um valor entre {0} e {1}."),
	max: $.validator.format("Digite um valor menor ou igual a {0}."),
	min: $.validator.format("Digite um valor maior ou igual a {0}."),
	nifES: "Digite um NIF v&aacute;lido.",
	nieES: "Digite um NIE v&aacute;lido.",
	cifEE: "Digite um CIF v&aacute;lido."
});

}));