﻿$('.fancybox').fancybox();

$(document).ready(function () {
    selecionarChip();
    abrirModalChipDuvida();
    habilitarBotaoContinuar();
    habilitarBotaoContinuarModal();
    botaoModalValido();
    AjustaBatataFrita();
   
    $('.owl-carousel').owlCarousel({
        dots: true,
        loop: false,
        items: 1,
        startPosition: 2,
        autoWidth: true,        
     
    });
});

function AjustaBatataFrita() {
    var browser = navigator.userAgent;
    if (browser.indexOf('Firefox') != -1) {
        $('.logo-controle-giga').addClass('logo-controle-giga-firefox');
    }
}

function selecionarChip () {
    $input = $('.form-chip-box');
    $input.click(function () {
        $(this).css('background-color', '#609')
            .css('border-color', '#390454')
            .css('box-shadow', '0px 0px 0px 3px #390454')
            .find('.form-chip__titulo').css('color', '#fff');
        $(this).find('.form-chip__img-chip--cinza').hide();
        $(this).find('.form-chip__img-chip--branco').show();

        $inputsNotChecked = $('.form-chip-box input:not(:checked)').parent();
        $inputsNotChecked.css('background-color', '#fff')
            .css('border-color', '#c8c8c8')
            .css('box-shadow', 'none')
            .find('.form-chip__titulo').css('color', '#828282');
        $inputsNotChecked.find('.form-chip__img-chip--cinza').show();
        $inputsNotChecked.find('.form-chip__img-chip--branco').hide();
    });
}

function abrirModalChipDuvida() {
    $('#abrirModalChip, .modal-form-chip__cancelar, .modal-form-chip-backdrop').click(function () {
        $('body').toggleClass('overflow-hidden');
        $('.modal-form-chip-backdrop').toggle();
        $('.modal-form-chip').toggle();
    });
}

function habilitarBotaoContinuar() {
    $('.form-chip-box').click(function () {
        $checked = $('.form-chip__input:checked');
        if ($checked) {
            $('.tipo-chip__btn-continuar').addClass('tipo-chip__btn-continuar--ativo').prop('disabled', false);
        }
    });
}

function habilitarBotaoContinuarModal() {
    $(document).on('click', '.ui-menu-item', function (e) {
        var val = $('.modal-form-chip__input').data('value');
        if (val == 1) {
            $('.modal-form-chip__selecionar').addClass('modal-form-chip__selecionar--ativo').prop('disabled', false);
        }
    });
}

function botaoModalValido() {
    $btn = $('.modal-form-chip__selecionar');
    $btn.click(function (e) {
        var val = $('.modal-form-chip__input').attr('value');
        if (val == '') {
            $btn.removeClass('modal-form-chip__selecionar--ativo').prop('disabled', true);
        }
    });
}

$('.btndetalhes').click(function () {
    $('.listainfo').slideToggle('slow');
   
});
 